provider "aws" {
  region = "us-west-2"
}

module "eks_dev" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = "dev-cluster"
  cluster_version = "1.21"
  subnets         = var.subnets
  vpc_id          = var.vpc_id

  node_groups = {
    dev_nodes = {
      desired_capacity = 1
      max_capacity     = 2
      min_capacity     = 1
      instance_type    = "t3.medium"
      key_name         = var.key_name
    }
  }

  tags = {
    Environment = "development"
  }
}
