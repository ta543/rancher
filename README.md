# 🚀 Scalable Multi-Cluster Application Deployment

## 📝 Project Overview
This repository provides a comprehensive guide for setting up and managing a multi-cluster Kubernetes environment using Rancher. With automation provided by Terraform for infrastructure provisioning and GitLab CI/CD for continuous integration and deployment, this project aims to demonstrate a robust DevOps pipeline suitable for development, staging, and production environments.

## 🌟 Features
- **Multi-Cluster Management**: Utilize Rancher to efficiently manage multiple Kubernetes clusters.
- **Infrastructure as Code (IaC)**: Provision and manage cloud infrastructure using Terraform.
- **Continuous Integration & Deployment**: Implement automated testing and deployment with GitLab CI/CD.
- **Monitoring & Logging**: Integrate Prometheus and Grafana for monitoring; Fluentd and ELK stack for comprehensive logging.
- **Security**: Ensure enhanced security with RBAC and network policies.

## ✅ Prerequisites
Make sure you have the following tools installed:
- Docker 🐳
- Kubernetes CLI (`kubectl`)
- Terraform 🌍
- Access to a GitLab instance
- Access to a Kubernetes cluster

## 🛠 Installation

### 1️⃣ Clone the Repository
```bash
git clone https://gitlab.com/ta543/rancher.git
cd rancher
```

### 2️⃣ Initialize Terraform
Navigate to the Terraform directories and initialize them:

```bash
cd terraform/network
terraform init
terraform apply

cd ../clusters/dev
terraform init
terraform apply
```

### 3️⃣ Set Up CI/CD
Ensure your GitLab project is configured with the necessary environment variables:

`CI_REGISTRY_IMAGE`

Kubernetes cluster access configurations

### 4️⃣ Deploy Applications
Use the GitLab pipeline to deploy your applications, or manually trigger deployments via the GitLab UI.

## 🚀 Usage
After deploying, access your applications via the configured external endpoints. Manage your clusters seamlessly through Rancher's UI.

## 📜 License
This project is licensed under the MIT License. See the LICENSE.md file for more details.

